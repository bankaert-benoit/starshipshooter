import Mobile from './Mobile';

import imgSource from '../assets/images/flyingSaucer-petit.png';

const SaucerState = {ALIVE : 0, DEAD : 1};

export default class Saucer extends Mobile {

    constructor(x,y,w = 48,h = 36,hs = -3,vs = 0){
        super(x,y,imgSource,w,h,hs,vs);
        this.state = SaucerState.ALIVE;
    }

    died(){
        this.speedH = 0;
        this.speedV = 3;
        this.state = SaucerState.DEAD;
    }

    move(canvas){
        switch (this.state) {
            case SaucerState.ALIVE :
                this.x = Math.max(0,this.x + this.speedH);
                break;
            case SaucerState.DEAD :
                this.y = Math.min(canvas.height - this.height, this.y + this.speedV);
                break;
            default :
                return;
        }  
        
    }

}