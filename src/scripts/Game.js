import Saucer from "./Saucer";
import Shoot from "./Shoot";
import StarShip from "./StarShip";

export default class Game {

    constructor(canvas,score){
        this.canvas = canvas;
        this.context = this.canvas.getContext("2d");
        this.ship = new StarShip(40,this.canvas.height/2);
        this.saucers = new Array();
        this.score = score;
        this.shoots = new Array();
        this.raf = window.requestAnimationFrame(this.moveAndDrawGame.bind(this));
    }

    addSaucer(){
        this.saucers.push(new Saucer(this.canvas.width-48,this.rand()-36));
    }

    rand(){
        return Math.random() * this.canvas.height
    }

    removeSaucer(saucer){
        const idx = this.saucers.indexOf(saucer);
        if (idx > -1){
            this.saucers.splice(idx,1);
        }
    }

    addShoot(){
        this.shoots.push(new Shoot(this.ship.x + this.ship.width ,this.ship.y + (this.ship.height/2)));
    }

    removeShoot(shoot){
        const idx = this.shoots.indexOf(shoot);
        if (idx > -1){
            this.shoots.splice(idx,1);
        }
    }

    updateScore(point){
        this.score.textContent = parseInt(this.score.textContent) + point;
    }

    moveAndDrawGame(){
        this.context.clearRect(0,0,this.canvas.width, this.canvas.height);
        this.checkShoot(); 
        this.ship.move(this.canvas);
        this.ship.draw(this.context);
        this.checkSaucer();
        this.raf = window.requestAnimationFrame(() => {
            this.moveAndDrawGame();
        });
    }

    checkSaucer(){
        this.saucers.forEach(s => {
            s.move(this.canvas);
            s.draw(this.context);
            if(s.x == 0){
                this.removeSaucer(s);
                this.updateScore(-1000);
            } else if(s.y == this.canvas.height - s.height && s.state == 1){
                this.removeSaucer(s);
            }          
        });
    }
    
    checkShoot(){
        this.shoots.forEach(shoot => {
            shoot.move(this.canvas);
            shoot.draw(this.context);
        });
        this.shoots.forEach(shoot => {
            if(shoot.x == this.canvas.width - shoot.width) {
                this.removeShoot(shoot);
            }else {
                const res = shoot.checkCollide(this.saucers);
            if(res !== null){
                res.died();
                this.updateScore(200);
                this.removeShoot(shoot);
            }
            }
       });   
    }


    controllShip(event){
        switch (event.key){
            case "ArrowUp" :
            case "z" :
                this.ship.moveUp();
                break;
            case "ArrowDown" :
            case "s" :
                this.ship.moveDown();
                break;
            case " ":
                this.addShoot();
            default :
                return;    
        }
        event.preventDefault();
    }

    stopControllShip(event){
        switch (event.key){
            case "ArrowUp" :
            case "z" :
            case "ArrowDown" :
            case "s" :
                this.ship.stopMoving();
                break;
            default :
                return;    
        }
        event.preventDefault();
    }
}