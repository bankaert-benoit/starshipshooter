import Mobile from './Mobile';

import imgSource from '../assets/images/vaisseau-ballon-petit.png';

const MoveState = {UP : 0, DOWN : 1, NONE : 2};

export default class StarShip extends Mobile{
    
    constructor(x,y,w = 48, h = 39, hs = 0, vs = 8){
        super(x,y,imgSource,w,h,hs,vs);
        this.moving = MoveState.NONE;
    }

    up(){
        return this.moving === MoveState.UP;
    }

    down(){
        return this.moving === MoveState.DOWN;
    }

    moveUp(){
        this.moving = MoveState.UP;
    }

    moveDown(){
        this.moving = MoveState.DOWN;
    }

    stopMoving(){
        this.moving = MoveState.NONE;
    }

    move(canvas){
        if(this.up()){
            this.y = Math.max(0, this.y - this.speedV);
        }else if(this.down()){
            this.y = Math.min(canvas.height - this.height, this.y + this.speedV);
        }
    }

}